Scenario: User account creation
  Given The driver is in the home page
  When The button "Entrar/Registar nova conta” is clicked
  And A division is shown with the login and the registrations inputs
  And The user send the keys  with the new username and password
  And The user send the keys  with the new username and the password
  And The user clicks on button "Registar nova conta"
  Then The window  must display the username on the top right corner

Scenario: Login performing
  Given The driver is in the home page
  When The button "Entrar/Registar nova conta” is clicked
    | naveenk | test@123 |
  And A division is shown with the login and the registrations inputs
  And The user send the keys with an existing username and password
  And The user clicks on button "Log in"
  Then The window must display the username on the top right corner
  And A division is shown with the login and the registrations inputs
  And The user send the keys with an existing username and the password
  And The user clicks on button "Log in"
  Then The window  must display the username on the top right corner

Scenario: Search by animal type
  Given The driver is in the home page
  When The user clicks on the button with tha animal type category
  Then A set of products that concerns the chosen type of animal are displayed

Scenario: Search by brand
  Given The driver is in the home page
  When The user move the mouse up to the text "Marcas"
  And The user clicks on one of the brands
  Then A set of products that concerns the chosen brand are displayed

Scenario: Utilization of filters on the research
  Given The driver is in the home page
  When The user clicks on the button with tha animal type category
  And The user choose one or more of the categories
  And The user choose one or more of the brands
  Then A set of products corresponding to the categories and brands chosen are displayed

Scenario: Sort the set of results
  Given The driver is in the home page
  When The user clicks on the button with tha animal type category
  And The user clicks on the text "Ordenação Padrão"
  And The user choose one of the sort types
  Then the set of results must be sorted by the chosen sort type

Scenario: Sort the set of results
  Given The driver is in the home page
  When The user send the keys for the product to search on the dialog box aside of the magnifying glass
  And The user clicks on the magnifying glass
  Then the expected product must be shown

Scenario: Add products to cart
  Given The driver is in the home page
  When The user clicks on the button with tha animal type category
  And The user click on one of the displayed products
  Then The product details  must be shown
  When The user click on "Adicionar"
  And The user clicks on "Carrinho"
  Then The added product must be present on the cart